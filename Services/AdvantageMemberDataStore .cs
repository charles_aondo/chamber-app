using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using CHA.Models;
using Plugin.Connectivity;
/**
 * This file will be used to store and access Advantage Members (Deals) information.
 * 
 * Author: Gideon Landry (BTC)
 */
[assembly: Xamarin.Forms.Dependency(typeof(CHA.Services.AdvantageMemberDataStore))]
namespace CHA.Services
{
    public class AdvantageMemberDataStore : IDataStore<AdvantageMember>
    {
        List<AdvantageMember> items;
        private ConnectionsUtil Util = new ConnectionsUtil();
        
        //EnvironmentVariableTarget Db = new SQLiteConnection();
        public AdvantageMemberDataStore()
        {
            items = new List<AdvantageMember>();
            try
            {
                if ((Util.Db.Table<AdvantageMember>().CountAsync().IsFaulted || Util.Db.Table<AdvantageMember>().CountAsync().Result == 0) && CrossConnectivity.Current.IsConnected)
                    Task.Run(async () => await Util.GetAdvantageMembersAsync(this));
                else
                    items = Util.Db.QueryAsync<AdvantageMember>("SELECT * FROM AdvantageMember GROUP BY Title").Result;
            }
            catch(Exception e)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    Task.Run(async () => await Util.GetAdvantageMembersAsync(this));
                }
            } 
        }
        public async Task<bool> AddItemAsync(AdvantageMember item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemsAsync(IEnumerable<AdvantageMember> advantages)
        {
            items.Clear();
            //foreach(var deal in advantages)
            //{

            items = advantages.ToList();

            //}
            //try
            //{

            //    var connection = new SQLiteAsyncConnection("");
            //    connection.CreateTableAsync<AdvantageMember>();
            //    return "Database created";
            //}
            //catch (SQLiteException ex)
            //{
            //    return ex.Message;
            //}
            //await Util.GetAdvantageMembersAsync(this);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(AdvantageMember item)
        {
            var _item = items.Where((AdvantageMember arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(AdvantageMember item)
        {
            var _item = items.Where((AdvantageMember arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<AdvantageMember> GetItemAsync(int id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<AdvantageMember>> GetItemsAsync(bool forceRefresh = false)
        {

            if ((items.Count() == 0 || forceRefresh) && CrossConnectivity.Current.IsConnected)
                await Util.GetAdvantageMembersAsync(this);

            return await Task.FromResult(items);
        }

        public Task<IEnumerable<Member>> GetItemsByFirstLetterAsync(string letter)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Member>> GetItemsByContainsAsync(string search)
        {
            throw new NotImplementedException();
        }
    }
}