using CHA.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

using Plugin.Connectivity;
/**
 * This file will be used to store and access Event information.
 * 
 * Author: Gideon Landry (BTC)
 */
[assembly: Xamarin.Forms.Dependency(typeof(CHA.Services.EventDataStore))]
namespace CHA.Services
{
    public class EventDataStore : IDataStore<Event>
    {
        List<Event> items;
        private ConnectionsUtil Util = new ConnectionsUtil();

        public EventDataStore()
        {
            //Ceereating a list
            items = new List<Event>();
            try
            {
                //
                if ((Util.Db.Table<Event>().CountAsync().IsFaulted || Util.Db.Table<Event>().CountAsync().Result == 0) && CrossConnectivity.Current.IsConnected)
                    Task.Run(async () => await Util.GetEventsAsync(this));
                else
                    items = Util.Db.QueryAsync<Event>("SELECT * FROM Event GROUP BY EventName").Result;
            }
            catch (Exception e)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    Task.Run(async () => await Util.GetEventsAsync(this));
                }
            }
        } 
            public async Task<bool> AddItemAsync(Event item)
            {
                items.Add(item);

                return await Task.FromResult(true);
            }
            public async Task<bool> UpdateItemsAsync(IEnumerable<Event> events)
            {
                items.Clear();
                items = events.ToList();
                //items = await Util.GetMembersAsync(Items);
                return await Task.FromResult(true);
            }
            public async Task<bool> DeleteItemAsync(Event item)
            {
                var _item = items.Where((Event arg) => arg.Id == item.Id).FirstOrDefault();
                items.Remove(_item);

                return await Task.FromResult(true);
            }

            public async Task<Event> GetItemAsync(int id)
            {
                return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
            }

            public async Task<IEnumerable<Event>> GetItemsAsync(bool forceRefresh = false)
            {
                if ((items.Count == 0 || forceRefresh) && CrossConnectivity.Current.IsConnected)
                    await Util.GetEventsAsync(this);

                return await Task.FromResult(items);
            }

            public Task<bool> UpdateItemAsync(Event item)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Member>> GetItemsByFirstLetterAsync(string letter)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Member>> GetItemsByContainsAsync(string search)
            {
                throw new NotImplementedException();
            }
        }

}
