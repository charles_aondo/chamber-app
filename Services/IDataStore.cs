﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CHA.Models;
/**
 * This is an interface that will be used to create all the DataStore files. 
 * All DataStore files will be required to implement each of the Tasks in this file.
 */
namespace CHA.Services
{
    public interface IDataStore<T>
    {
        Task<bool> AddItemAsync(T item);
        Task<bool> UpdateItemAsync(T item);
        Task<bool> DeleteItemAsync(T item);
        Task<T> GetItemAsync(int id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
        Task<bool> UpdateItemsAsync(IEnumerable<T> items);
        Task<IEnumerable<Member>> GetItemsByFirstLetterAsync(string letter);
        Task<IEnumerable<Member>> GetItemsByContainsAsync(string search);
    }
}
