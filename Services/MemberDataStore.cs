using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using CHA.Models;
using Newtonsoft.Json;
using SQLite;

using Plugin.Connectivity;

[assembly: Xamarin.Forms.Dependency(typeof(CHA.Services.MemberDataStore))]
namespace CHA.Services
{
    /**
     * This page holds local Member info
     * Author: Gideon Landry (BTC)
     * Since: February 26, 2018
     */
    public class MemberDataStore : IDataStore<Member>
    {  
        //private ObservableCollection<MemberGrouping> Groups { get; set; }
        public bool Updating { get; set; }
        //Creating a database connection
        private ConnectionsUtil Util = new ConnectionsUtil();

        //Creating a list object to store the data collected from the Util Method
        List<Member> Items;
        public MemberDataStore()
        {
            //Making an instant of the list object
            Items = new List<Member>();

            //Fetching the members from the api collected array in the connection.util for display to the screen for the user
            try
            {
                //Checking for Database connections
                if ((Util.Db.Table<Member>().CountAsync().IsFaulted || Util.Db.Table<Member>().CountAsync().Result == 0) && CrossConnectivity.Current.IsConnected)
                {
                    Task.Run(async () => await Util.GetMembersAsync(this));
                   
                }
                else
                {
                    //Querying the database for the data
                    Items = Util.Db.QueryAsync<Member>("SELECT * FROM Member").Result;

                }
            }
            catch (Exception e)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    Task.Run(async () => await Util.GetMembersAsync(this));
                }
            }
        }

        /**
         * Charles Aondo
         * Date:26-02-2019
         * Purpose:This methods are used to update the app database using the list collectd above
         **/

        public async Task<bool> AddItemAsync(Member item)
        {
            Items.Add(item);
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemsAsync(IEnumerable<Member> members)
        {
            Items.Clear();
            Items = members.ToList();
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Member item)
        {
            var _item = Items.Where((Member arg) => arg.MemId == item.MemId).FirstOrDefault();
            Items.Remove(_item);
            Items.Add(item);
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(Member item)
        {
            var _item = Items.Where((Member arg) => arg.MemId == item.MemId).FirstOrDefault();
            Items.Remove(_item);
            return await Task.FromResult(true);
        }

        public async Task<Member> GetItemAsync(int id)
        {
            return await Task.FromResult(Items.FirstOrDefault(s => s.MemId == id));
        }
        
        public async Task<IEnumerable<Member>> GetItemsAsync(bool forceRefresh = false)
        {
            if ((Items.Count == 0 || forceRefresh) && CrossConnectivity.Current.IsConnected)
                await Util.GetMembersAsync(this);

            return await Task.FromResult(Items);
        }
        public async Task<IEnumerable<Member>> GetItemsByFirstLetterAsync(string letter)
        {
            if (letter.Equals("#"))
                letter = "[0 - 9]";
            else letter = letter.ToUpper();
            return await Task.FromResult(Util.Db.QueryAsync<Member>("SELECT * FROM Member WHERE DisplayName LIKE \'" + letter + "%\'").Result);
        }

        public async Task<IEnumerable<Member>> GetItemsByContainsAsync(string search)
        {
            if (Items.Count == 0)
                await Util.GetMembersAsync(this);

            return await Task.FromResult(Util.Db.QueryAsync<Member>("SELECT * FROM Member WHERE DisplayName LIKE \'%" + search + "%\'").Result);
        }
    }
  
}