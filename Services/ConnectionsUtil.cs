﻿using System;
using System.Collections.Generic;
using CHA.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using SQLite;
using System.Text;
using System.IO;
using System.Net;
using Xamarin.Forms;
using System.Collections.ObjectModel;

/**
* This file will hold all utilities for dbecting to an online source to get Members, News, and Events.
* Author: Gideon Landry (BTC)
* Since: March 14, 2018
* Modified: April 26, 2018
* 
*/
namespace CHA.Services
{
    class ConnectionsUtil
    {
        //local SQLite database.
        public SQLiteAsyncConnection Db { get; set; }
        private Uri url;
        private MemberDataStore membersStore;
        private AdvantageMemberDataStore advMembersStore;
        private EventDataStore eventDataStore;

    
        public ConnectionsUtil()
        {
            Db = new SQLiteAsyncConnection(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "myDB.db"));
        }       
        public ConnectionsUtil(string path)
        {
            url = new Uri(path);
           
            
        }


        public async Task<bool> GetMembersAsync(MemberDataStore store)
        {
            string json = "";

            if (!store.Updating)
            {

                store.Updating = true;
                //Creating the sqlite database table
                await Db.QueryAsync<Member>("DROP TABLE IF EXISTS Member").ContinueWith(t =>
                {
                    Db.CreateTableAsync<Member>();

                    Console.WriteLine("Members table created!");
                });

                Console.WriteLine("Gideon: Attempting to get members.");
                try
                {
                    //Calling the api url created by Gideon n Co to fetch members
                    url = new Uri("http://54.39.180.209/members");

                    using (WebClient sr = new WebClient())
                    {
                        Console.WriteLine("Charles: Retrieving Members....");
                        //Downloading the data from  the api
                        sr.DownloadStringCompleted += async (s, e) =>
                        {
                            Console.WriteLine("Charles: Members Downloaded. Processing Members....");
                            json = e.Result;
                                //Deserialising the json array and storing it into a list 
                                //The list is now stored into the database
                                //Query updates the items from the database
                                await Db.InsertAllAsync(JsonConvert.DeserializeObject<IEnumerable<Member>>(json)).ContinueWith(async t =>
                                   await store.UpdateItemsAsync(Db.QueryAsync<Member>("SELECT * FROM Member GROUP BY DisplayName").Result)
                               );
                            Console.WriteLine("Charles: Finished processing members.");
                            membersStore = store;                      
                            
                            store.Updating = false;
                        };
                        sr.DownloadStringAsync(url);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Charles: An error occured while trying to dbect. ERROR: " + e);
                    store.Updating = false;
                    return await Task.FromResult(false);
                }
            }
            return await Task.FromResult(true);
        }

        //this will get all Advantage Members (Deals) from database.
        public async Task GetAdvantageMembersAsync(AdvantageMemberDataStore store)
        {
            await Db.QueryAsync<AdvantageMember>("DROP TABLE IF EXISTS AdvantageMember").ContinueWith(t =>
            {

                Db.CreateTableAsync<AdvantageMember>();

                Console.WriteLine("AdvantageMember table created!");

            });
            url = new Uri("http://54.39.180.209/benefits");

            Console.WriteLine("Gideon: Attempting to get advantage members.");
            try
            {

                string json;
                //Gideon: get json string containing advantage members.
                using (WebClient sr = new WebClient())
                {

                    Console.WriteLine("Gideon: Retrieving advantage Members....");

                    sr.DownloadStringCompleted += async (s, e) =>
                    {

                        Console.WriteLine("Gideon: Advantage Members Downloaded. Processing advantage Members....");
                        json = e.Result;
                        //Deserialising the json array and storing it into a list 
                        //The list is now stored into the database
                        //Query updates the items from the database
                        await Db.InsertAllAsync(JsonConvert.DeserializeObject<IEnumerable<AdvantageMember>>(json)).ContinueWith(async t =>
                            {
                                await store.UpdateItemsAsync(Db.QueryAsync<AdvantageMember>("SELECT * FROM AdvantageMember GROUP BY Title").Result);
                            });
                            Console.WriteLine("Processing Members benefits for android");
                            advMembersStore = store;
                    };
                    await Task.Run(() => sr.DownloadStringAsync(url));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Gideon: An error occured while trying to dbect. ERROR: " + e);
            }
        }
     
        /*
       * Name:Charles Aondo
       * Date: 2018-03-06
       * Purpose: This the method that fetches the event from  the API and update the page on the screen 
       */
        //this will get all chamber/member events from database.
        public async Task GetEventsAsync(EventDataStore store)
        {

            url = new Uri("http://54.39.180.209/events");

            Console.WriteLine("Gideon: Attempting to get events.");
            try
            {
                await Db.QueryAsync<Event>("DROP TABLE IF EXISTS Event").ContinueWith(t =>
                {
                    Db.CreateTableAsync<Event>();
                
                    Console.WriteLine("Event table created!");
                });
                string json;
                //Gideon: get json string containing advantage members.
                using (WebClient sr = new WebClient())
                {

                    Console.WriteLine("Gideon: Retrieving events....");

                    sr.DownloadStringCompleted += async (s, e) =>
                    {

                        Console.WriteLine("Gideon: events Downloaded. Processing them....");
                        json = e.Result;
                       
                             IEnumerable<Event> events = JsonConvert.DeserializeObject<IEnumerable<Event>>(json);
                            await store.UpdateItemsAsync(events);
                            eventDataStore = store;


                        Console.WriteLine("Gideon: Finished processing events.");

                    };
                    await Task.Run(() => sr.DownloadStringAsync(url));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Gideon: An error occured while trying to dbect. ERROR: " + e);

            }
        }   
/*
 * Name:Charles Aondo
 * Date: 2018-03-06
 * Purpose: This the method that sends the card details the user entered in the loginPage.xaml  to the API 
 */
        public async Task<bool> RequestCard(CardRequest request)
        {
            try
            {
               HttpClient client = new HttpClient();
                //https://stackoverflow.com/questions/19610883/sending-c-sharp-object-to-webapi-controller
                //Collecting the users data posted from the login page
                var values = new Dictionary<string, string>()
                {
                    {"name",request.name},
                    {"email",request.email},
                    {"company", request.company},
                };
                //Encording the values so the can be psoted
                var content = new FormUrlEncodedContent(values);
                //Api url 
                string apiUrl = "http://54.39.180.209/register";
                //Posting the values to the api 
                var response = await client.PostAsync(apiUrl, content);
                //This method returns show the details and shows true if the the request was sent sucessfully
                response.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                Console.WriteLine("Gideon: Error caught. Error: " + e);
                return false;
            }        
            return true;      
        }
    }
}
