﻿using System;

using CHA.Models;

namespace CHA.ViewModels
{
    public class EventDetailViewModel : BaseViewModel
    {
        public Event Event { get; set; }
        
        public EventDetailViewModel(Event @event = null)
        {
            Title = @event?.EventName;
            Event = @event;
        }
    }
}
