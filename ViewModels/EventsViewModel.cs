﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using CHA.Models;
using CHA.Views;
using System.Linq;

namespace CHA.ViewModels
{
    public class EventsViewModel : BaseViewModel
    {
        public ObservableCollection<Event> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public EventsViewModel()
        {
            Title = "Events";

            Items = new ObservableCollection<Event>();


            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            Task.Run(async () =>
            {
                var events = await EventDataStore.GetItemsAsync();
                foreach (var @event in events)
                    Items.Add(@event);
                Console.WriteLine("CU" + Items.Count());


                //Sort by order of date
                var temp = Items.OrderBy(x => x.EventTime).ToList();
                Items.Clear();
                var current = 0;
                foreach (Event item in temp)
                {
                    //Set the color here as we're reordering the list
                    if (current % 2 == 0)
                    {
                        item.Background = Color.FromHex("#f9f9f9");

                    }
                    else
                    {
                        item.Background = Color.FromHex("#eae8e8");
                    }
                    Items.Add(item);

                    Console.WriteLine("Debugging Events" + Items.ToArray());
                    current++;

                }


            });
        }

        //Executing the load command instance
        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await EventDataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }

                //Sort by order of date
                var temp = Items.OrderBy(x => x.EventTime).ToList();
                Items.Clear();
                var current = 0;
                foreach (Event item in temp)
                {
                    //Set the color here as we're reordering the list
                    if (current % 2 == 0)
                    {
                        item.Background = Color.FromHex("#f9f9f9");
                    }
                    else
                    {
                        item.Background = Color.FromHex("#eae8e8");
                    }

                    Items.Add(item);
                    current++;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
    }





    