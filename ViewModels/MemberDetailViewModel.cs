﻿using System;

using CHA.Models;

namespace CHA.ViewModels
{
    public class MemberDetailViewModel : BaseViewModel
    {
        public Member Member { get; set; }
        public MemberDetailViewModel(Member member = null)
        {
            Title = member?.DisplayName;
            Member = member;
        }
    }
}
