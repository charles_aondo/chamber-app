﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using CHA.Models;
using System.Timers;
using System.Linq;
using System.Collections.Generic;
/**
* This page will provide MembersPage access to members (list)
* Author: Gideon Landry (BTC)
*/
namespace CHA.ViewModels
{
    //Implements MBaseViewModel
    public class MembersViewModel : BaseViewModel
    {
        //declare objects with setters and getters.
        public ObservableCollection<Member> Members { get; set; }
        public ObservableCollection<Grouping<string, Member>> MembersGrouped { get; set; }
        public ObservableCollection<Grouping<string, Member>> Grouped { get; set; }
        public ObservableCollection<MemberGrouping> Groups { get; set; }
        public Command LoadMembersCommand { get; set; }
        public Command FilterMembersCommand { get; set; }
        public string Text { get; set; }

        private bool checker;
        //private bool inProgress;

        public MembersViewModel()
        {
            //declare a Members object array
            Members = new ObservableCollection<Member>();
            MembersGrouped = new ObservableCollection<Grouping<string, Member>>();

            //Groups = new ObservableCollection<MemberGrouping>();


            //connect to MemberDataStore service and get all local Members.
            LoadMembersCommand = new Command(async () => await ExecuteLoadMembersCommand());

            //when a filter request is made run Filter Members Command.
            FilterMembersCommand = new Command(async () => await ExecuteFilterMembersCommand());

            Task.Run(async () =>
            {
                var members = await MemberDataStore.GetItemsAsync();
                foreach (var member in members)
                    Members.Add(member);

                var sorted = from member in Members
                             orderby member.DisplayName.TrimStart()
                             group member by member.NameSort into memberGroup
                             select new Grouping<string, Member>(memberGroup.Key, memberGroup);
              
                Grouped = new ObservableCollection<Grouping<string, Member>>(sorted);
                foreach (var member in Grouped)
                    MembersGrouped.Add(member);
                    
            });
            async Task ExecuteFilterMembersCommand()
            {

                if (IsBusy)
                    return;

                IsBusy = true;



                try
                {

                    this.Text = Text.Trim(); //Trim whitespace from searchterm endings

                    if (Text == "")
                    {
                        IsBusy = false;
                        await ExecuteLoadMembersCommand();
                        return;
                    }
                    //create an array of groups for each letter of the alphabet.

                    //var members = await MemberDataStore.GetItemsAsync(false);

                    MembersGrouped.Clear();
                    Members.Clear();

                    //if search bar text is a single character search for members beginning with that character.
                    if (Text.Length == 1)
                    {
                        var members = await MemberDataStore.GetItemsByFirstLetterAsync(Text);
                        if (members.ToList().Count > 0)
                        {
                            foreach (var item in members)
                            {
                                //if(item.DisplayName[0].ToString().ToUpper().Equals(Text.ToUpper()))
                                Members.Add(item);
                            }
                            var sorted = from member in Members
                                         orderby member.DisplayName.TrimStart()
                                         group member by member.NameSort into memberGroup
                                         select new Grouping<string, Member>(memberGroup.Key, memberGroup);
                            Grouped = new ObservableCollection<Grouping<string, Member>>(sorted);
                            foreach (var member in Grouped)
                                MembersGrouped.Add(member);
                        }
                    } //if search bar text contains more than one character, search for members whose display Name contains search bar text.
                    else
                    {
                        var members = await MemberDataStore.GetItemsByContainsAsync(Text);
                        foreach (var member in members)
                        {
                            Members.Add(member);                
                        }
                        var sorted = from member in Members
                                     orderby member.DisplayName.TrimStart()
                                     group member by member.NameSort into memberGroup
                                     select new Grouping<string, Member>(memberGroup.Key, memberGroup);
                        Grouped = new ObservableCollection<Grouping<string, Member>>(sorted);
                        foreach (var member in Grouped)
                            MembersGrouped.Add(member);
                        IsBusy = false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
            async Task ExecuteLoadMembersCommand()
            {
                //if already busy cancel. prevents multiple instances.
                if (IsBusy)
                    return;
                //mark as busy
                IsBusy = true;
                //if the members are filtered run filter command instead.
                if (Text != "")
                {
                    //updates the local Members in the data store. 

                    IsBusy = false;
                    await ExecuteFilterMembersCommand();
                    return;
                }      
                try
                {
                    //clears Members collection on this page.
                    Members.Clear();
                    MembersGrouped.Clear();

                    var items = await MemberDataStore.GetItemsAsync(true);

                    foreach (var item in items)
                    {
                        Members.Add(item);
                    }
                    var sorted = from member in Members
                                 orderby member.DisplayName.TrimStart()
                                 group member by member.NameSort into memberGroup
                                 select new Grouping<string, Member>(memberGroup.Key, memberGroup);
                    Grouped = new ObservableCollection<Grouping<string, Member>>(sorted);
                    foreach (var member in Grouped)
                        MembersGrouped.Add(member);


                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public class Grouping<K, T> : ObservableCollection<T>
        {
            public K Key { get; private set; }

            public Grouping(K key, IEnumerable<T> items)
            {
                Key = key;
                foreach (var item in items)
                    this.Items.Add(item);
            }
        }
    }
}