﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using CHA.Models;
using CHA.Views;

namespace CHA.ViewModels
{
    public class AdvantageMembersViewModel : BaseViewModel
    {
        public ObservableCollection<AdvantageMember> AdvantageMembers { get; set; }
        public Command LoadItemsCommand { get; set; }

        public AdvantageMembersViewModel()
        {
            AdvantageMembers = new ObservableCollection<AdvantageMember>();
            Title = "Advantage Members";
            AdvantageMembers = new ObservableCollection<AdvantageMember>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            Task.Run(async()=> 
            {
                var deals = await AdvantageMemberDataStore.GetItemsAsync();
                foreach (var deal in deals)
                    AdvantageMembers.Add(deal);
            });         
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                //await AdvantageMemberDataStore.UpdateItemsAsync();

                AdvantageMembers.Clear();

                var items = await AdvantageMemberDataStore.GetItemsAsync(true);
                
                foreach (var item in items)
                {
                    //item.CompanyToCity = item.OrganizationName + " - " + item.City;
                    AdvantageMembers.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}