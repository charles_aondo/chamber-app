﻿using System;

using CHA.Models;

namespace CHA.ViewModels
{
    public class AdvantageMemberDetailViewModel : BaseViewModel
    {
        public AdvantageMember Member { get; set; }
        
        public AdvantageMemberDetailViewModel(AdvantageMember member = null)
        {
            Title = member?.DisplayName;
            Member = member;
        }
    }
}
