﻿using CHA.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CHA.ViewModels
{
    public class CarouselViewModel
    { 

        public ObservableCollection<Carousel> Imgs { get; set; }

        public CarouselViewModel()
        {

            Imgs = new ObservableCollection<Carousel>
            {

                new Carousel
                {
                    ImgUrl = "http://charlottetownchamber.com/wp-content/uploads/belong-to-win-2018.jpg"
                },
                 new Carousel
                {
                    ImgUrl = "http://charlottetownchamber.com/wp-content/uploads/GCACC-BAH-Web-Banner_Update.jpg"
                },
                  new Carousel
                {
                    ImgUrl = "http://charlottetownchamber.com/wp-content/uploads/GCACC-Excellence-Student-Award-Web-Banner.jpg"
                },
                   new Carousel
                {
                    ImgUrl = "http://charlottetownchamber.com/wp-content/uploads/ROI-slide3.jpg"
                }

            };
        }
    }
}

