﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json;

/**
 * This page will provide a view of the Charlottetown Chambers Member Card, or, if the user is not registered, provide a registration form to request Member Card.
 * 
 * Author: Gideon Landry (BTC)
 * Since: March ‎28, ‎2018
 * Modified by: Charles Aondo
 */
namespace CHA.Views
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public  partial class CardPage : ContentPage
	{
        private Frame card = new Frame()
        {
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Fill,
        };

        private bool confirmedStatus = false;   
        Services.ConnectionsUtil Util = new Services.ConnectionsUtil();
        Entry cardNumber = new Entry() { Placeholder = "Card Number:     000000", };  
        public CardPage()
        {      
            try
            {
                //attempt to get card request object from SQLite.
                        var user = Util.Db.GetAsync<Models.CardRequest>(pk: 1).Result;
                        Content = CardView(user);  
            }
            catch (Exception e)
            {
                //If user information isn't known, allow the user to enter it
                StackLayout info = new StackLayout();
                var infoLabel = new Label() { Text = "Please Enter Your Details Below" };
                info.Children.Add(infoLabel);
                var Email = new Entry() { Placeholder = "Email" };
   
                info.Children.Add(Email);
           
                info.Children.Add(cardNumber);
                var Submit = new Button() { Text = "Submit" };        
                info.Children.Add(Submit);
                Submit.BackgroundColor = Color.FromHex("#3fa5ba");
                Submit.BorderColor = Color.FromHex("#91e6f7");
                Submit.BorderWidth = 2;
                Submit.TextColor = Color.White;

                //Creating the cardRequest Instant
                //var request = new Models.CardRequest();

                Submit.Pressed += async (sender, ee) =>
                {
                    //Validate every input
                    if (!string.IsNullOrEmpty(Email.Text) && !string.IsNullOrEmpty(cardNumber.Text))
                    {
                        //Validating the user email input if its correct and Key using Regex
                        if ((Regex.Match(Email.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success) && (Regex.Match(cardNumber.Text, @"\b\d{6}\b").Success))
                        {
                            //Collecting the variables to be posted to the API after validation
                            var values = new Dictionary<string, string>()
                                {
                               
                                    {"email", Email.Text},   
                                    {"cardNumber",cardNumber.Text},
                                };

                            var content = new FormUrlEncodedContent(values);
                          
                            //Api url 
                            string apiUrl = "http://54.39.180.209/validate";
                            //Making http request
                            HttpClient client = new HttpClient();                   
                                //Posting the values to the api 
                                var response = await client.PostAsync(apiUrl, content);
                         
                                //This method returns show the details and shows true if the the request was sent sucessfully
                                response.EnsureSuccessStatusCode();
                                Console.WriteLine("Charles Awaiting Return value from API............. " + response);
                                //Collecting the return value from the api
                                var statusjson = await response.Content.ReadAsStringAsync();
                               //Deserialiozing the json object
                               var apiReturnValues = JsonConvert.DeserializeObject<Models.CardRequest>(statusjson);
                               Console.WriteLine("Charles Recieving from api.............. " + apiReturnValues.status);
                              confirmedStatus = apiReturnValues.status;                    
                            //Creating the object
                            //Storing the api return variable in a way that it will be saved during both onstart, ondead and onresume
                            Application.Current.Properties["status"] = apiReturnValues.status;
                          
                            var request = new Models.CardRequest()
                            {
                                Pk = 1,
                                email = Email.Text,
                                Confirmed = Application.Current.Properties["status"].ToString(),                         
                        };
                            //Updating the database with the b
                            await Util.Db.DropTableAsync<Models.CardRequest>();
                            await Util.Db.CreateTableAsync<Models.CardRequest>();
                            await Util.Db.InsertAsync(request);

                            Console.WriteLine("Status Report..........................." + confirmedStatus);
                            if (confirmedStatus == true)
                            {
                                Console.WriteLine("Charles Aondo showing card..............");                  
                                //Show card
                                info.Children.Add(new Image() { Source = "MemberCard.png", HorizontalOptions = LayoutOptions.FillAndExpand });
                                //Hide input fields                   
                                info.Children.Remove(Email);         
                                info.Children.Remove(cardNumber);
                                info.Children.Remove(infoLabel);
                                Submit.IsVisible = false;
                            }

                          if(confirmedStatus == false)
                            {
                                //when the credentials do not match that of the API and the  staus report is false
                                infoLabel.Text = "Invalid Credentials! Please Check your Details";
                                infoLabel.FontSize = 18;
                                infoLabel.TextColor = Color.Red;
                            }
                        }
                        else
                        {
                            infoLabel.Text = "Please enter a valid Email address/Card Number";
                            infoLabel.FontSize = 18;
                            infoLabel.TextColor = Color.Red;
                            Email.TextColor = Color.Red;
                        }
                    }
                   
                    else
                    {
                        infoLabel.Text = "Please Enter all fields";
                        infoLabel.FontSize = 18;
                        infoLabel.TextColor = Color.Red;
                    }
                    //Displaying the card to the user
                    
                };
                //if data submitted successfully, remove the button
                card.Content = info;
                Content = card;
                InitializeComponent();
        
            }
          
        }
    /**
     * Name:Charles Aondo
     * Date:2018-03-08
     * Purpose: This method will display the card if the user has already logged in
     **/
        private Frame CardView(Models.CardRequest request)
            {
                StackLayout stack = new StackLayout();
            //if user is already confirmed display card.
            if (Application.Current.Properties.ContainsKey("status"))
            {
                Console.WriteLine("Charles Printing shared Reference in the other key............." + request.Confirmed);

                if (request.Confirmed.Equals("True"))
                {
                    stack.Children.Add(new Image() { Source = "MemberCard.png", HorizontalOptions = LayoutOptions.CenterAndExpand });        
                } // if user exist but is not yet confirmed display key entry form.
                else
                {
                    var keyLabel = new Label() { Text = "Enter the Key we emailed you." };
                    stack.Children.Add(keyLabel);
                    stack.Children.Add(cardNumber);
                    //code to run when submit key field is filled in 
                }
                }
                card.Content = stack;
                return card;
            }         
        }
    }
