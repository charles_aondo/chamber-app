﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Connectivity;
/**
 * This page will allow a user to register for a card.
 * 
 * Author: Gideon Landry (BTC)
 * Since: March ‎28, ‎2018 
 *    
 */
namespace CHA.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        private Frame card = new Frame()
        {
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Fill
        };

        Services.ConnectionsUtil Util = new Services.ConnectionsUtil();
		public LoginPage ()
		{
            InitializeComponent();
		}

        //activates when Register for Card button is pressed.
        public void OnSubmit(object sender, ClickedEventArgs args)
        {
            //check if all fields are filled, if not notify user.
            if ((Content.FindByName<Entry>("Name").Text != null) && (Content.FindByName<Entry>("Email").Text != null) && (Content.FindByName<Entry>("Company").Text != null) )
            {
                Content.FindByName<Entry>("Email").TextColor = Color.Default;

                //check if Email is proper format.
                if (Regex.Match(Content.FindByName<Entry>("Email").Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        Content.FindByName<Label>("Error").IsVisible = false;

                        //create request object
                        var request = new Models.CardRequest()
                        {
                            Pk = 1,
                            company = Content.FindByName<Entry>("Company").Text,
                            email = Content.FindByName<Entry>("Email").Text,
                            name = Content.FindByName<Entry>("Name").Text,
                        
                        };
                      
                        //send request to Chambers
                        Task.Run(async () =>
                        {
                            await Util.RequestCard(request);
                        });
                        Content.FindByName<Button>("Register").IsVisible = false;
                        Content.FindByName<Entry>("Company").IsVisible = false;
                        Content.FindByName<Entry>("Email").IsVisible = false;
                        Content.FindByName<Entry>("Name").IsVisible = false;

                        //Display a green success message
                        Content.FindByName<Label>("Error").IsVisible = true;
                        Content.FindByName<Label>("Error").TextColor = Color.DarkGreen;
                        Content.FindByName<Label>("Error").Text = "Successfully sent request.\nPlease wait for an approval Email.";
                        Content.FindByName<Label>("Error").FontSize = 20;
                        Content.FindByName<Label>("Error").HorizontalTextAlignment = TextAlignment.Center;
                    }
                    else
                    {
                        Content.FindByName<Label>("Error").Text = "Error: Please check your internet connection";
                    }
                }
                else
                {
                    Content.FindByName<Entry>("Email").TextColor = Color.Red;
                }            
            } 
            else
            {
                Content.FindByName<Entry>("Email").TextColor = Color.Default;
                Content.FindByName<Label>("Error").IsVisible = true;
                Content.FindByName<Label>("Error").Text = "Please fill in all fields";
            }
        }
    }
}