﻿using CHA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {
       public BaseViewModel Model { get; set; }
        public TabbedPage1 ()
        {

            Model = new BaseViewModel();
            var test = new MembersViewModel();
            this.Children.Add(new NavigationPage(new MyCarousel()) { Icon = "ic_home.png", Title = "Home" });
            this.Children.Add(new NavigationPage(new MembersPage()) {Icon = "ic_group.png", Title = "Memb."});
            this.Children.Add(new NavigationPage(new AdvantageMembersPage()) {Icon = "ic_redeem.png", Title = "Deals"});
            this.Children.Add(new NavigationPage(new EventsPage()) {Icon = "ic_event.png", Title = "Events"});
            this.Children.Add(new NavigationPage(new CHA.Views.TabbedPageLog()) { Icon = "ic_account_box.png", Title = "Card" });
            InitializeComponent();

        }
    }
}