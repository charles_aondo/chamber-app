﻿using CHA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CHA.Models;
using CHA.Views;
using Xamarin.Forms.Maps;

using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
/**
* This is the processing code for the Advantage Member Detail page.
* Author: Gideon Landry (BTC)
* Since: February 28, 2018
* Modified by:Charles Aondo
*/
namespace CHA.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AdvantageMemberDetailPage : ContentPage
	{

        AdvantageMemberDetailViewModel viewModel;
        //Member member;
		public AdvantageMemberDetailPage(AdvantageMemberDetailViewModel viewModel)
		{
            
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
            //member = new Member();

            //Task.Run(async () => member = await viewModel.MemberDataStore.GetItemAsync(viewModel.Member.Id));

            //add Adv. Member description aa html source.
            HtmlWebViewSource html = new HtmlWebViewSource
            {

                Html = viewModel.Member.Description

            };

            //create a web view that will display Adv. Member description with functioning html.
            WebView webView = new WebView()
            {

                Source = html

            };

            //when a link in the description is pressed open in device default browser instead of within this app.
            webView.Navigating += (s, e) =>
                {
                    if (e.Url.StartsWith("http"))
                    {
                        try
                        {
                            var uri = new Uri(e.Url);
                            Device.OpenUri(uri);
                        }
                        catch (Exception)
                        {
                        }

                        e.Cancel = true;
                    }
                };

            //var MapTab = new ContentPage() { Title = "Map", };
            var layout = new StackLayout();

          
            Content = webView;
        }

        public AdvantageMemberDetailPage()
        {

            //create new Member object.
            var member = new AdvantageMember
            {

            };

            viewModel = new AdvantageMemberDetailViewModel(member);
            BindingContext = viewModel;
        }

        public void WebViewChange()
        {


        }
    }
}