﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CHA.Models;
using CHA.Views;
using CHA.ViewModels;
using System.Timers;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
/**
* This is the processing code behind the MembersPage.xaml.
* Author: Gideon Landry (BTC)
* Since: February 26, 2018
*/
namespace CHA.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MembersPage : ContentPage
	{
        MembersViewModel viewModel;

        public MembersPage()
        {
            //start loading page.
            InitializeComponent();
            BindingContext = viewModel = new MembersViewModel();
        
        }
        
        //Runs when search bar text changes.
        private void SearchTextChanged(object sender, TextChangedEventArgs args)
        {
            if (viewModel.IsBusy)
                return;

            //send search text to view model.
            viewModel.Text = args.NewTextValue as string;
            MembersListView.Header = "";

            //If the search bar is empty we want to reset the list
            if(args.NewTextValue == "") {
                viewModel.LoadMembersCommand.Execute(null);
            }

        }

        //Runs when search button on keyboard is pressed
        private void SearchButtonPressed(object sender)
        {
            //run view model filter command.
            viewModel.FilterMembersCommand.Execute(null);


            if (viewModel.Text != "")
            {

                //if search text is a single character long, results are for members beginning with that character.
                if (viewModel.Text.Length == 1)
                    MembersListView.Header = "Results beginning with \"" + viewModel.Text + "\"";

                //if search text is more than 1 character long, results are for members whos Name contains the characters.
                else
                    MembersListView.Header = "Results containing \"" + viewModel.Text + "\"";

            }
            else
                MembersListView.Header = null;
        }

        //when a member is selected run this method.
        async void OnMemberSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var member = args.SelectedItem as Member;
            if (member == null)
                return;

            await Navigation.PushAsync(new MemberDetailPage(new MemberDetailViewModel(member)));

            // Manually deselect member.
            MembersListView.SelectedItem = null;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            if (viewModel.Members.Count == 0)
            {
                viewModel.Text = "";
                viewModel.LoadMembersCommand.Execute(null);
         
            }                          
        }
        public void ListStyling(object sender, ElementEventArgs args)
        {

            //args.Element.;
            //MembersListView.
        }
    }
}