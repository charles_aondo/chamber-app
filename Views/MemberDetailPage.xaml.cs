﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CHA.Models;
using CHA.Views;
using CHA.ViewModels;
using Xamarin.Forms.Maps;

using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
/**
* This is the processing code for the Member Detail page.
* Author: Gideon Landry (BTC)
* Since: February 28, 2018
* Modified: Charles Aondo
*/
namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MemberDetailPage : TabbedPage
    {
        //sets ViewModel
        MemberDetailViewModel viewModel;

        public MemberDetailPage(MemberDetailViewModel viewModel)
        {
            InitializeComponent();


            //connects to xaml code 
            BindingContext = this.viewModel = viewModel;

            var About = new MemberAboutPage { Title = "About" };
            var layout = new StackLayout();

            var MapTab = new MapPage() { Title = "Map" };


            var line1 = new Label() { Text = viewModel.Member.Line1, FontSize = 16, HorizontalTextAlignment = TextAlignment.Center };
            var status = CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location).Result;
            //Checking the users phone on android  to see and return an error if their location is not
            //turned on.
            if (Device.RuntimePlatform == Device.Android && status != PermissionStatus.Granted)
            {
                var infoLabel = new Label() { Text = "Please Enable Location Permission for this App to view Maps" };
                infoLabel.FontSize = 18;
                infoLabel.TextColor = Color.Red;
                MapTab.Content = layout;
                this.Children.Add(MapTab);
                layout.Children.Add(infoLabel);
            }
            else
            {
                //Making sure that map does not show if that member has no longitude and latitude
                if (viewModel.Member.Latitude != 0.0 && viewModel.Member.Longitude != 0.0)
                {
                    Position pos = new Position(viewModel.Member.Latitude, viewModel.Member.Longitude);
                    Map map = new Map(

                        MapSpan.FromCenterAndRadius(

                            pos,
                            Distance.FromMiles(0.3)))
                    {
                        //Showing the map and customizing the view
                        IsShowingUser = true,
                        HeightRequest = 100,
                        WidthRequest = 960,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };

                    Pin pin = new Pin
                    {

                        Type = PinType.Place,
                        Position = pos,
                        Label = viewModel.Member.DisplayName,
                        Address = viewModel.Member.Line1 + " " + viewModel.Member.City + ", " + viewModel.Member.Region + " " + viewModel.Member.PostalCode,

                    };
                    map.Pins.Add(pin);
                    //var stack = new StackLayout { Spacing = 0 };
                    layout.Children.Add(map);

                    MapTab.Content = layout;
                    this.Children.Add(MapTab);

                }
            }
        }
    }
}