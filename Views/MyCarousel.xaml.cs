﻿using CHA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyCarousel : CarouselPage
    {
        //Frame which will hold images / information
        private Frame frame1 = new Frame()
        {
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.FillAndExpand
        };

        private Frame frame2 = new Frame()
        {
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.CenterAndExpand
        };

        private Frame frame3 = new Frame()
        {
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.CenterAndExpand
            
        };

        public MyCarousel ()
        {
            //About text to be seen under each image
            var about = "\nThe Greater Charlottetown Area Chamber of Commerce is a non-profit organization made up of business and professional people sharing a common goal: the economic development of the capital region.";
            var about2 = "\nWith close to 1,000 members, the chamber reflects a diverse network of small, medium and large businesses from almost every industry sector and business profession.\n";
            var about3 = "\nThrough service on the Chamber board, a Chamber committee or a task force, members express their views, and direct and shape the activities and programs of the Chamber.\n";

            //First Page
            StackLayout slide1 = new StackLayout();
           
            slide1.Children.Add(new Image
            {
                Source = "newlogo.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            slide1.Children.Add(new Image
            {
                Source = "s1.jpg",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            slide1.Children.Add(new Label { Text = about, FontSize = 16 });
            slide1.Children.Add(new Image
            {
                Source = "d1.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
          
            frame1.Content = slide1;
            var content1 = new ContentPage { Content = frame1 };
            Children.Add(content1);
            
            //Second Page
            StackLayout slide2 = new StackLayout();
            slide2.Children.Add(new Image
            {
                Source = "newlogo.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            slide2.Children.Add(new Image
            {
                Source = "s2.jpg",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            slide2.Children.Add(new Label { Text = about2, FontSize = 16 });
            slide2.Children.Add(new Image
            {
                Source = "d2.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            
            frame2.Content = slide2;   
            var content2 = new ContentPage { Content = frame2 };
            Children.Add(content2);

            //Third Page
            StackLayout slide3 = new StackLayout();
            slide3.Children.Add(new Image
            {
                Source = "newlogo.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            slide3.Children.Add(new Image
            {
                Source = "s3.jpg",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
            slide3.Children.Add(new Label { Text = about3, FontSize = 16 });
            slide3.Children.Add(new Image
            {
                Source = "d3.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            });
          
            frame3.Content = slide3;
            var content3 = new ContentPage { Content = frame3 };
            
            Children.Add(content3);

        }
    }
}