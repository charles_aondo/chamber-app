﻿using CHA.Models;
using CHA.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdvantageMembersPage : ContentPage
    {
        AdvantageMembersViewModel viewModel;

        public AdvantageMembersPage()
        {

            InitializeComponent();

            BindingContext = viewModel = new AdvantageMembersViewModel();
        }

        async void OnAdvantageMemberSelected(object sender, SelectedItemChangedEventArgs args)
        { 
            //Get the item selected and convert to AdvantageMember object.
            var advMember = args.SelectedItem as AdvantageMember;
            if (advMember == null)
                return;

            await Navigation.PushAsync(new AdvantageMemberDetailPage(new AdvantageMemberDetailViewModel(advMember)));

            //Deselect Item
            AdvantageMembersListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.AdvantageMembers.Count == 0)
            {
                //AdvantageMembersListView.IsRefreshing = true;

                viewModel.LoadItemsCommand.Execute(null);

                //create timer object with 1 second count down.
                var timer = new Timer(1000);

                int i = 0;
                //declare what the timer will do when it finishes.
                timer.Elapsed += (sender, e) =>
                { 
                    
                    //if advantage members are found or 10 seconds have passed stop timer.
                    if (i >= 10 || viewModel.AdvantageMembers.Count > 0)
                    {
                        timer.Stop();
                        //AdvantageMembersListView.IsRefreshing = false;

                        //set ListView IsRefreshing property back to IsBusy Binding.
                        //AdvantageMembersListView.SetBinding(ListView.IsRefreshingProperty, new Binding("IsBusy", BindingMode.OneWay));
                    }
                    i++;

                    viewModel.LoadItemsCommand.Execute(null);

                };

                //start timer. Timer repeats until timer.stop() is called.
                timer.Start();
                
                
            }
        }
    }
}
