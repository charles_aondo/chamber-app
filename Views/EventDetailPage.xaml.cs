﻿using CHA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventDetailPage : ContentPage
    {
        EventDetailViewModel viewModel;
        public EventDetailPage (EventDetailViewModel viewModel)
        {

            InitializeComponent();
            BindingContext = this.viewModel = viewModel;

            //create a web view that will display Adv. Member description with functioning html.
            WebView DescriptionWebView = new WebView() {
                Source =  new HtmlWebViewSource { Html = viewModel.Event.Description },
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };        
            //when a link in the description is pressed open in device default browser instead of within this app.
            DescriptionWebView.Navigating += (s, e) =>
            {
                if (e.Url.StartsWith("http"))
                {
                    try
                    {
                        var uri = new Uri(e.Url);
                        Device.OpenUri(uri);
                    }
                    catch (Exception)
                    {
                    }

                    e.Cancel = true;
                }
            };

            StackLayout stack = new StackLayout();

            //Only create a register button if there is a registration url set. Some events do not have registrations
            if(Uri.IsWellFormedUriString(viewModel.Event.EventRegistrationURL, UriKind.Absolute))
            {
                var Register = new Button {
                    Text = "Register For Event",
                    HorizontalOptions = LayoutOptions.Center,
                    BackgroundColor = Color.FromHex("#3fa5ba"),
                    BorderColor = Color.FromHex("#91e6f7"),
                    BorderWidth = 2,
                    TextColor = Color.White
                };
                Register.Pressed += (sender, ee) =>
                {
                    Device.OpenUri(new Uri(viewModel.Event.EventRegistrationURL));
                };
                stack.Children.Add(Register);
            }

            stack.Children.Add(DescriptionWebView);

       


            var descriptionContain = new ContentPage()
            {
                Title = "Details",
                Content = stack
            };
            Content = stack;
         
        }
    }
}