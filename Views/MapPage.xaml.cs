﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
/*
 * Name: Charles Aondo
 * Date:2019-04-25
 * Purpose: This page is use to mirror the map tab in the memberdetail page so that it will be customizable using 
 * the renderes in the ios tab
 */
namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
       
    }
}