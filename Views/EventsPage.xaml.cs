﻿using CHA.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
//using static Android.App.Usage.UsageEvents;

namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventsPage : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }
        public WebView Web { get; set; }
        EventsViewModel viewModel;

        public EventsPage()
        {

            InitializeComponent();

            BindingContext = viewModel = new EventsViewModel();

        }

        async void OnEventSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var @event = args.SelectedItem as Models.Event;
            if (@event == null)
                return;

            await Navigation.PushAsync(new EventDetailPage(new EventDetailViewModel(@event)));

            // Manually deselect member.
            EventsListView.SelectedItem = null;

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
            {
                //show loading icon in list.
                //MembersListView.IsRefreshing = true;
                int i = 0;

                //create timer object with 1 second count down.
                var timer = new Timer(1000);

                //declare what the timer will do when it finishes.
                timer.Elapsed += (sender, e) =>
                {

                    //if members are found or 10 seconds have passed stop timer.
                    if (i >= 10 || viewModel.Items.Count > 0)
                    {
                        timer.Stop();

                    }

                    i++;

                    viewModel.LoadItemsCommand.Execute(null);



                };

                //start timer. Timer repeats until timer.stop() is called.
                timer.Start();


            }


        }

    }
}
