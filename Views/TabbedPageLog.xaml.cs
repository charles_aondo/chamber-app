﻿using CHA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CHA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPageLog : TabbedPage
    {
       public BaseViewModel Model { get; set; }
        public TabbedPageLog ()
        {

            Model = new BaseViewModel();
            var test = new MembersViewModel();

            this.Children.Add(new Xamarin.Forms.NavigationPage(new LoginPage()) { Title = "Register" });
            this.Children.Add(new Xamarin.Forms.NavigationPage(new CardPage()) { Title = "View Card" });
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            InitializeComponent();

        }
    }
}