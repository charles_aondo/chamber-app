﻿using System;
using CHA.Services;
using CHA.Views;
using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Push;

namespace CHA
{
	public partial class App : Application
	{

		public App ()
		{
			InitializeComponent();
            var page = new TabbedPage1();
            page.CurrentPage = page.Children[0];
            //the first page that will load: AboutPage.xaml (Detail) with MasterdetailPage1Master.xaml (Master) as the left navigation menu.
            MainPage = page;
        }

		protected override void OnStart ()
		{
            AppCenter.Start("android=4788ef0e-b7fe-4b5f-a8da-30e18ce73d03;" +
                  "uwp={Your UWP App secret here};" +
                  "ios=ba9955ed-a447-4e7a-902c-c51b9419c973",
                  typeof(Analytics), typeof(Crashes), typeof(Push));

            // Handle when your app starts

            /*
             * when the app starts run ConnectionsUtil which will grab all the member, news, events
             * information from the Charlottetown Chamber's database and load it into the app.
             */
            //new ConnectionsUtil();
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
            /*
             * want to do something here, not sure what though.
             */
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
            /*
             * when the app resumes update local storage information.
             */
		}
	}
}
