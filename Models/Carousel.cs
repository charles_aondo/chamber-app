﻿using System;
using System.Collections.Generic;
using System.Text;
/**
 * 
 * Author: Gideon Landry (BTC)
 * Since: March 5, 2018
 */
namespace CHA.Models
{
    public class Carousel
    {

        public string ImgUrl { get; set; }

    }
}
