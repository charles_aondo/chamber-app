﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace CHA.Models
{
    public class CardRequest
    { 
        [PrimaryKey]
        public int Pk { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string company { get; set; }
        public bool status { get; set; }
        public string Confirmed { get; set; }
    }
}
