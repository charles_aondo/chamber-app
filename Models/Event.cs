﻿using System;
using Xamarin.Forms;
/**
 * This class holds attributes for events.
 * Author: Gideon Landry (BTC)
 * Since: February 27, 2018
 */
namespace CHA.Models
{
    public class Event
    {
        private int current = 0;

        public int Id { get; set; }

        public string EventName { get; set; }
        public string EventLink { get; set; }
        public string Description { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string RegistrationCutoff { get; set; }
        public string EventLocation { get; set; }
        public string EventRegistrationURL { get; set; }
        public string Hours { get; set; }
        public string Admission { get; set; }
        public Color Background{ get; set; }
        public DateTime EventTime
        {
            get
            {
                return Convert.ToDateTime(StartTime);
            }
        }
    }
}
