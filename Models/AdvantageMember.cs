﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
/**
 * This class holds attributes for advantage member deals.
 * Author: Gideon Landry (BTC)
 * Since: February 27, 2018
 */
namespace CHA.Models
{
    public class AdvantageMember
    {
        private static int count = 0;
        private int current = 0;
        public AdvantageMember()
        {
            count++;
            current = count;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string ImageURL { get; set; }
        public int MemberId { get; set; }
        public int TypeId { get; set; }
        public string TagLine { get; set; }
        public string DisplayName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Color Background
        {
            get
            {
                if (current % 2 == 0)
                {
                    //return Color.FromHex("#3fa5ba");
                    return Color.FromHex("#eae8e8");
                }
                else return Color.FromHex("#f9f9f9");
            }
        }
    }
}
