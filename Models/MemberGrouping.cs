﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CHA.Models
{
    public class MemberGrouping : ObservableCollection<Member>
    {

        public String Name { get; private set; }
        public String ShortName { get; private set; }

        public MemberGrouping (String Name, String ShortName)
        {
            this.Name = Name;
            this.ShortName = ShortName;
        }

    }
}
