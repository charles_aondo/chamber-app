﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
/**
 * This class holds attributes for Charllottetown Chamber members.
 * Author: Gideon Landry (BTC)
 * Since: February 27, 2018
 * Modified by: Charles Aondo
 * 
 * Resource: http://api.micronetonline.com/V1/documentation/Api/GET-associations(associationId)-members-details
 */
namespace CHA.Models
{
   
    public class Member
    {
        
        [JsonProperty(PropertyName = "SearchDescription")]
        public string SearchDescription { get; set; }

        [JsonProperty(PropertyName = "MemId"), PrimaryKey]
        public int MemId { get; set; }

        //added new
        [JsonProperty(PropertyName = "ChamberRepId", NullValueHandling = NullValueHandling.Ignore)]
        public int ChamberRepId { get; set; }

        //added new
        [JsonProperty(PropertyName = "PrimaryRepId", NullValueHandling = NullValueHandling.Ignore)]
        public int PrimaryRepId { get; set; }

        //added new
        [JsonProperty(PropertyName = "StatusType")]
        public int StatusType { get; set; }

        //added new
        [JsonProperty(PropertyName = "ShowOnMap")]
        public bool ShowOnMap { get; set; }

        //Added new 
        [JsonProperty(PropertyName = "SearchLogoUrl")]
        public string SearchLogoUrl { get; set; }

        //Added new 
        [JsonProperty(PropertyName = "LogoUrl")]
        public string LogoUrl { get; set; }

        [JsonProperty(PropertyName = "Website")]
        public string Website { get; set; }

        [JsonProperty(PropertyName = "DispPhone2")]
        public string DispPhone2 { get; set; }

        [JsonProperty(PropertyName = "DispFax")]
        public string DispFax { get; set; }

        [JsonProperty(PropertyName = "DispPhone1")]
        public string DispPhone1 { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "DisplayName")]
        public string DisplayName { get; set; }

        [JsonProperty(PropertyName = "Region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "Line1")]
        public string Line1 { get; set; }

        [JsonProperty(PropertyName = "Line2")]
        public string Line2 { get; set; }

        [JsonProperty(PropertyName = "PostalCode")]
        public string PostalCode { get; set; }

        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        //The nullValueHandling is use to ignore null values if that object does have longitude and latitude
        [JsonProperty(PropertyName = "Longitude", NullValueHandling = NullValueHandling.Ignore)]  
        public double Longitude { get; set; }

        [JsonProperty(PropertyName ="Latitude", NullValueHandling = NullValueHandling.Ignore)]
        public double Latitude { get; set; }

        public String NameSort
        { 
            get
            {
                return DisplayName.TrimStart()[0].ToString().ToUpper();
            }
        }

    }
}
